
/* Copyright INRIA 2007 */

package org.scilab.modules.shell;

/**
 * Scilab Bridge for Scilab Shell object
 * @author Vincent COUVERT
 *
 */
public class ScilabShellBridge {

	/**
	 * Constructor for ScilabShellBridge class
	 */
	protected ScilabShellBridge() {
		throw new UnsupportedOperationException(); /* Prevents calls from subclass */
	}

	/**
	 * Displays a line in the Shell
	 * @param lineToPrint the line to be printed
	 */
	public static void printf(String lineToPrint) {
		// TODO Auto-generated method stub
	}

	/**
	 * Clear the shell
	 */
	public static void clear() {
		// TODO Auto-generated method stub
	}

	/**
	 * Get the number of lines displayed
	 * @return the number of lines displayed
	 */
	public static int lines() {
		// TODO Auto-generated method stub
		return 0;
	}

	/**
	 * Reads a line in the Shell
	 * @return the line read
	 */
	public static String readLine() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Put the cursor to the top-left position
	 */
	public static void tohome() {
		// TODO Auto-generated method stub
		
	};

}
