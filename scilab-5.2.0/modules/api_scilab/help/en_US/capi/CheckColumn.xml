<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="CheckColumn" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>CheckColumn</refname>

    <refpurpose>C interface function which checks if a parameter send to the C
    function is a column vector or not</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>CheckColumn(StackPos,m_var,n_var)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>StackPos</term>

        <listitem>
          <para>the position in the Scilab memory of the argument for which we
          want to perform the check (input parameter)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>m_var</term>

        <listitem>
          <para>the number of lines of the parameter at position StackPos in
          the Scilab memory</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>n_var</term>

        <listitem>
          <para>the number of columns of the parameter at position StackPos in
          the Scilab memory</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>C interface function which checks if a parameter send to the C
    function is a column vector or not. You must include stack-c.h to benefit
    from this function.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>In this example, the C interface function takes one input parameters
    and prints the integer corresponding to the type of the variable sent as
    parameter in the Scilab console. If the test fails, we return from the C
    interface and an adequate error message is printed in the Scilab
    console.</para>

    <programlisting role="example"><![CDATA[ 
#include <stack-c.h>

int sci_check_properties(char * fname)
{
  int m1, n1, l1;

  CheckRhs(1,1);

  GetRhsVar(1, "d", &m1, &n1, &l1);

  CheckColumn(1,m1,n1); // Check that first argument is a column vector

  return 0;
}
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="CheckDims">CheckDims</link></member>

      <member><link linkend="CheckRow">CheckRow</link></member>

      <member><link linkend="CheckScalar">CheckScalar</link></member>

      <member><link linkend="CheckVector">CheckVector</link></member>

      <member><link linkend="OverLoad">CheckOverLoad</link></member>

      <member><link linkend="CheckDimProp">CheckDimProp</link></member>

      <member><link linkend="CheckLength">CheckLength</link></member>

      <member><link linkend="CheckSameDims">CheckSameDims</link></member>

      <member><link linkend="CheckSquare">CheckSquare</link></member>

      <member><link linkend="HowToCheckParameters">How to check
      parameters</link></member>
    </simplelist>
  </refsection>
</refentry>
