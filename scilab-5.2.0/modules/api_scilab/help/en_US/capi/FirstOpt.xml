<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="FirstOpt" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>FirstOpt</refname>

    <refpurpose>C gateway function which returns the position of the first
    optional parameter</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>Pos FirstOpt()</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>Pos</term>

        <listitem>
          <para>the position of the first optional parameter, Rhs + 1 if no
          optional parameters have been given to the function</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>A C gateway function which returns the position of the first
    optional parameter. You must include stack-c.h to benefit from this
    function.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>A more complete example is available in the directory
    SCI/modules/core/example/optional_parameters.</para>

    <programlisting role="example"><![CDATA[ 
#include <stack-c.h>

int sci_optional_parameters(char * fname)
{ 
  int m1,n1,l1;

  // optional names must be stored in alphabetical order in opts
  static rhs_opts opts[]= {{-1,"v1","d",0,0,0},
                           {-1,"v2","d",0,0,0},
                           {-1,NULL,NULL,0,0}};

  int minrhs = 1, maxrhs = 1;
  int minlhs = 1, maxlhs = 3;
  int nopt, iopos, res;
  char buffer_name[csiz]; // csiz used for character coding

  nopt = NumOpt();

  CheckRhs(minrhs,maxrhs+nopt);
  CheckLhs(minlhs,maxlhs);

  // first non optional argument
  GetRhsVar( 1, "c", &m1, &n1, &l1);
  
  if (get_optionals(fname,opts)==0) return 0;

  sciprint("number of optional parameters = %d\n", NumOpt());
  sciprint("first optional parameters = %d\n", FirstOpt());
  sciprint("FindOpt(v1) = %d\n", FindOpt("v1", opts));
  sciprint("FindOpt(v2) = %d\n", FindOpt("v2", opts));

  if (IsOpt(1,buffer_name))
    sciprint("parameter 1 is optional: %s\n", buffer_name);
  if (IsOpt(2,buffer_name))
    sciprint("parameter 2 is optional: %s\n", buffer_name);
  if (IsOpt(3,buffer_name))
    sciprint("parameter 3 is optional: %s\n", buffer_name);

  return 0;
}
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="CheckDims">CheckDims</link></member>

      <member><link linkend="CheckRow">CheckRow</link></member>

      <member><link linkend="CheckScalar">CheckScalar</link></member>

      <member><link linkend="CheckVector">CheckVector</link></member>

      <member><link linkend="OverLoad">CheckOverLoad</link></member>

      <member><link linkend="CheckDimProp">CheckDimProp</link></member>

      <member><link linkend="CheckLength">CheckLength</link></member>

      <member><link linkend="CheckSameDims">CheckSameDims</link></member>

      <member><link linkend="CheckSquare">CheckSquare</link></member>

      <member><link linkend="HowToCheckParameters">How to check
      parameters</link></member>
    </simplelist>
  </refsection>
</refentry>
