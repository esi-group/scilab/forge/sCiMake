<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="OverLoad" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv>
    <refname>OverLoad</refname>

    <refpurpose>C gateway function which tells Scilab to look for another
    overloaded function</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>OverLoad(StackPos)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>StackPos</term>

        <listitem>
          <para>the position in the Scilab memory of the parameter for which
          we want to take into account for the overloading process (input
          argument)</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>A C gateway function which tells Scilab to look for another
    overloaded function. Scilab then appends to the name of the function a
    prefix (like %sp_ if the parameter taken into account is sparse) and look
    for the overloaded function. You must include stack-c.h to benefit from
    this function. </para>

    <para>Be careful with the Scilab name of the function. Indeed, the current
    overloading process of Scilab works only on Scilab primitives (Scilab
    function wrote in C) which must have a Scilab name which is maximum 8 char
    wide.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <para>In this example, the C interface function takes one input parameters
    and prints the integer corresponding to the type of the variable sent as
    parameter in the Scilab console.</para>

    <programlisting role = "example"><![CDATA[  
#include <stack-c.h>
#include <sciprint.h>

int sci_check_properties_2(char * fname)
{
  int m1,n1,l1;

  CheckRhs(1,1);
  CheckLhs(0,1) ;

  switch(VarType(1)) 
    {
    case sci_matrix: 
      GetRhsVar(1, "d", &m1, &n1, &l1);
      sciprint("1 is a scalar matrix\n");
      break;
    case sci_strings:
      GetRhsVar(1, "c", &m1, &n1, &l1);
      sciprint("1 is a string\n");
      break;
    case sci_sparse:
      sciprint("1 is a sparse trying to overload\n");
      OverLoad(1);
    }

  LhsVar(1) = 0;

  return 0;
}
 </programlisting>

    <para>The builder.sce script look like this:</para>

    <programlisting role = "example"><![CDATA[  
// This is the builder.sce 
// must be run from this directory 

lines(0);

ilib_name  = 'lib_check_properties';

files = ['check_properties.c'];

libs  = [];

table =['chprop2', 'sci_check_properties_2'];

// We must be careful when we choose a scilab function name in case of overloading.
// We Scilab name function must be 8 char max.

ldflags = "";
cflags  = "";
fflags  = "";

ilib_build(ilib_name,table,files,libs,'Makelib',ldflags,cflags,fflags);
 ]]></programlisting>

    <para>And now, an example of use of this new function:</para>

    <programlisting role = "example"><![CDATA[  
chprop2([1,2,2]);
chprop2('foo');

// overload case 

deff('[]=%sp_chprop2(sp)','disp(''sparse overloaded'')');
chprop2(sparse([1,2,3]));
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="CheckColumn">CheckColumn</link></member>

      <member><link linkend="CheckDims">CheckDims</link></member>

      <member><link linkend="CheckRow">CheckRow</link></member>

      <member><link linkend="CheckScalar">CheckScalar</link></member>

      <member><link linkend="CheckVector">CheckVector</link></member>

      <member><link linkend="CheckDimProp">CheckDimProp</link></member>

      <member><link linkend="CheckLength">CheckLength</link></member>

      <member><link linkend="CheckSameDims">CheckSameDims</link></member>

      <member><link linkend="CheckSquare">CheckSquare</link></member>

      <member><link linkend="HowToCheckParameters">How to check
      parameters</link></member>
    </simplelist>
  </refsection>
</refentry>
