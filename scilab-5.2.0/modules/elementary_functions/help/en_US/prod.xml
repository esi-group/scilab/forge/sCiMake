<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="prod" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>prod</refname>

    <refpurpose>product</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>y=prod(x)
y=prod(x,'r') or y=prod(x,1)
y=prod(x,'c') or y=prod(x,2)
y=prod(x,'m')</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>x</term>

        <listitem>
          <para>real or complex vector or matrix</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>y</term>

        <listitem>
          <para>real or complex scalar or matrix</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>For a vector or a matrix <literal>x</literal>,
    <literal>y=prod(x)</literal> returns in the scalar <literal>y</literal>
    the prod of all the entries of <literal>x</literal>, , e.g.
    <literal>prod(1:n)</literal> is n!</para>

    <para><literal>y=prod(x,'r')</literal> (or, equivalently,
    <literal>y=prod(x,1)</literal>)computes the rows elementwise product of
    <literal>x</literal>. <literal>y</literal> is the row vector:
    <literal>y(1,j)=prod(x(:,j))</literal>.</para>

    <para><literal>y=prod(x,'c')</literal> (or, equivalently,
    <literal>y=prod(x,2)</literal>) computes the columns elementwise product
    of <literal>x</literal>. <literal>y</literal> is the column vector:
    <literal>y(i,1)=prod(x(i,:))</literal>.</para>

    <para><literal>y=prod(x,'m')</literal> is the product along the first non
    singleton dimension of <literal>x</literal> (for compatibility with
    Matlab).</para>

    <para><literal>prod</literal> is not implemented for sparse
    matrices.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[  
A=[1,2;0,100];
prod(A)
prod(A,'c')
prod(A,'r')
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="sum">sum</link></member>

      <member><link linkend="cumprod">cumprod</link></member>
    </simplelist>
  </refsection>
</refentry>
