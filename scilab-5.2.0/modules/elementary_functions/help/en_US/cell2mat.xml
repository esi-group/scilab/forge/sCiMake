<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry version="5.0-subset Scilab" xml:id="cell2mat" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>

  <refnamediv>
    <refname>cell2mat</refname>

    <refpurpose>convert a cell array into a matrix</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>x=cell2mat(c)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>c</term>

        <listitem>
          <para>cell, the components of c must have the same type and can be
          scalars or matrices</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>x</term>

        <listitem>
          <para>matrix</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>Returns a matrix which is the concatenation of all components of the
    cell c.</para>

    <variablelist>
      <varlistentry>
        <term>cell2mat(c)</term>

        <listitem>
          <para>all components of c must have the same data type (strings or
          doubles or integers or booleans). For each row <literal>i
          </literal>of <literal>c,</literal> <literal>cell2mat</literal>
          concatenates all the components of the <literal>ith</literal> row of
          the cell <literal>c</literal></para>
        </listitem>
      </varlistentry>
    </variablelist>

    <para>Note that if the components of the cell input c<literal>
    </literal>are strings then <literal>cell2mat(c)</literal> returns a column
    vector of strings concatenation.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[ 
c=makecell([2,2],[1 2 3; 6 7 8],[4 5;9 10],[11 12;16 17],[14 13 15;18 19 20])
cell2mat(c)
 ]]></programlisting>
  </refsection>

  <refsection>
    <title>See Also</title>

    <simplelist type="inline">
      <member><link linkend="cell">cell</link></member>
    </simplelist>
  </refsection>
</refentry>
