Copyright:
Copyright (c) 2008 - DIGITEO
Copyright (c) 1989-2008 - INRIA

License:
This module must be used under the terms of the CeCILL.
This module file is licensed as described in the file COPYING, which
you should have received as part of this distribution.  The terms
are also available at
http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt

GL2PS: 
======
Files: src/c/gl2ps/gl2ps.c src/c/gl2ps/gl2ps.h

Copyright:
Copyright (C) 2003, Christophe Geuzaine

License: 
LGPL

See: http://www.geuz.org/gl2ps/COPYING.LGPL
