/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 1.3.40
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package org.scilab.modules.graphic_export;


 /** 
  * Call from the JoGL display thread to figure drawing code using JNI 
  * @author Jean-Baptiste Silvy
  * @copyright INRIA 2007
  */
public class GL2PSConstantJNI {

  /**
   * Constructor. Should not be called
   */
  protected GL2PSConstantJNI() {
	throw new UnsupportedOperationException();
  }

  static {
    try {
        System.loadLibrary("scigraphic_export");
    } catch (SecurityException e) {
		System.err.println("A security manager exists and does not allow the loading of the specified dynamic library :");
		e.printStackTrace(System.err);
	} catch (UnsatisfiedLinkError e)	{
		System.err.println("The native library scigraphic_export does not exist or cannot be found.");
		e.printStackTrace(System.err);
    }
  }

  public final static native int get_GL2PS_MAJOR_VERSION();
  public final static native int get_GL2PS_MINOR_VERSION();
  public final static native int get_GL2PS_PATCH_VERSION();
  public final static native String get_GL2PS_EXTRA_VERSION();
  public final static native int get_GL2PS_PS();
  public final static native int get_GL2PS_EPS();
  public final static native int get_GL2PS_TEX();
  public final static native int get_GL2PS_PDF();
  public final static native int get_GL2PS_SVG();
  public final static native int get_GL2PS_PGF();
  public final static native int get_GL2PS_NO_SORT();
  public final static native int get_GL2PS_SIMPLE_SORT();
  public final static native int get_GL2PS_BSP_SORT();
  public final static native int get_GL2PS_SUCCESS();
  public final static native int get_GL2PS_INFO();
  public final static native int get_GL2PS_WARNING();
  public final static native int get_GL2PS_ERROR();
  public final static native int get_GL2PS_NO_FEEDBACK();
  public final static native int get_GL2PS_OVERFLOW();
  public final static native int get_GL2PS_UNINITIALIZED();
  public final static native int get_GL2PS_NONE();
  public final static native int get_GL2PS_DRAW_BACKGROUND();
  public final static native int get_GL2PS_SIMPLE_LINE_OFFSET();
  public final static native int get_GL2PS_SILENT();
  public final static native int get_GL2PS_BEST_ROOT();
  public final static native int get_GL2PS_OCCLUSION_CULL();
  public final static native int get_GL2PS_NO_TEXT();
  public final static native int get_GL2PS_LANDSCAPE();
  public final static native int get_GL2PS_NO_PS3_SHADING();
  public final static native int get_GL2PS_NO_PIXMAP();
  public final static native int get_GL2PS_USE_CURRENT_VIEWPORT();
  public final static native int get_GL2PS_COMPRESS();
  public final static native int get_GL2PS_NO_BLENDING();
  public final static native int get_GL2PS_TIGHT_BOUNDING_BOX();
  public final static native int get_GL2PS_POLYGON_OFFSET_FILL();
  public final static native int get_GL2PS_POLYGON_BOUNDARY();
  public final static native int get_GL2PS_LINE_STIPPLE();
  public final static native int get_GL2PS_BLEND();
  public final static native int get_GL2PS_TEXT_C();
  public final static native int get_GL2PS_TEXT_CL();
  public final static native int get_GL2PS_TEXT_CR();
  public final static native int get_GL2PS_TEXT_B();
  public final static native int get_GL2PS_TEXT_BL();
  public final static native int get_GL2PS_TEXT_BR();
  public final static native int get_GL2PS_TEXT_T();
  public final static native int get_GL2PS_TEXT_TL();
  public final static native int get_GL2PS_TEXT_TR();
}
