# Localization of the module polynomials
# Please see in SCI/tools/localization for localization management
# Copyright (C) 2007/2008 - INRIA / Scilab
# This file is distributed under the same license as the Scilab package.
# Sylvestre Ledru <sylvestre.ledru@inria.fr>, 2007/2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Scilab\n"
"Report-Msgid-Bugs-To: <localization@scilab.org>\n"
"POT-Creation-Date: 2008-09-22 14:08+0100\n"
"Last-Translator: Sylvestre Ledru <sylvestre.ledru@inria.fr>\n"
"Language-Team: Scilab Localization <localization@scilab.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Revision-Date: 2009-12-09 16:34+0100\n"

# File: macros/polfact.sci, line: 29
#, c-format
msgid "%s: Input argument #%d must be real.\n"
msgstr ""
#
# File: macros/horner.sci, line: 57
#, c-format
msgid "%s: Unexpected type for input argument #%d.\n"
msgstr ""
#
# File: macros/horner.sci, line: 24
#, c-format
msgid "%s: Wrong number of input argument(s): %d expected.\n"
msgstr ""
#
# File: macros/factors.sci, line: 22
# File: macros/polfact.sci, line: 25
#, c-format
msgid "%s: Wrong size for input argument #%d: A polynomial expected.\n"
msgstr ""
#
# File: macros/factors.sci, line: 27
#, c-format
msgid "%s: Wrong size for input argument #%d: A rational fraction expected.\n"
msgstr ""
#
# File: macros/hrmt.sci, line: 18
#, c-format
msgid "%s: Wrong size for input argument #%d: A row vector expected.\n"
msgstr ""
#
# File: macros/factors.sci, line: 41
#, c-format
msgid ""
"%s: Wrong size for input argument #%d: A single input, single output system "
"expected.\n"
msgstr ""
#
# File: macros/coffg.sci, line: 23
# File: macros/coffg.sci, line: 36
# File: macros/determ.sci, line: 25
# File: macros/hermit.sci, line: 22
# File: macros/detr.sci, line: 19
# File: macros/detr.sci, line: 32
#, c-format
msgid "%s: Wrong size for input argument #%d: A square matrix expected.\n"
msgstr ""
#
# File: macros/coffg.sci, line: 47
# File: macros/numer.sci, line: 23
# File: macros/denom.sci, line: 27
# File: macros/invr.sci, line: 92
# File: macros/determ.sci, line: 22
# File: macros/derivat.sci, line: 42
# File: macros/detr.sci, line: 43
#, c-format
msgid ""
"%s: Wrong type for input argument #%d: A floating point number or polynomial "
"or rational fraction array expected.\n"
msgstr ""
#
# File: macros/factors.sci, line: 53
#, c-format
msgid ""
"%s: Wrong type for input argument #%d: Linear dynamical system or polynomial "
"array expected.\n"
msgstr ""
#
# File: macros/htrianr.sci, line: 20
# File: macros/polfact.sci, line: 22
# File: macros/hermit.sci, line: 18
#, c-format
msgid "%s: Wrong type for input argument: Polynomial array expected.\n"
msgstr ""
#
# File: macros/invr.sci, line: 56
# File: macros/invr.sci, line: 88
#, c-format
msgid "%s: Wrong value for input argument #%d: Must be in the set {%s}.\n"
msgstr ""
#
# File: macros/inv_coeff.sci, line: 24
#, c-format
msgid "%s: incompatible input arguments %d and %d\n"
msgstr ""
#
# File: macros/determ.sci, line: 58
msgid "Computing determinant: Be patient...\n"
msgstr ""
#
# File: etc/polynomials.start, line: 30
msgid "Polynomials"
msgstr ""
