FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

FILE(GLOB
  MEXLIB_C_SOURCES
  src/c/mexlib.c 
  src/c/sci_gateway.c
) 

LIST(APPEND MEXLIB_DEP
  dynamic_link
  core
  data_structures
  output_stream
)

ADD_LIBRARY(mex
  ${MEXLIB_C_SOURCES}
)

ADD_LIBRARY(mat
  ${MEXLIB_C_SOURCES}
)

ADD_LIBRARY(mx
  ${MEXLIB_C_SOURCES}
)

TARGET_LINK_LIBRARIES(mex
  ${MEXLIB_DEP}
)

TARGET_LINK_LIBRARIES(mat
  ${MEXLIB_DEP}
)

TARGET_LINK_LIBRARIES(mx
  ${MEXLIB_DEP}
)