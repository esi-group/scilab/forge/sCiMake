//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by scilab_windows.rc
//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) INRIA - Allan CORNET
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
#define IDD_STARTUPBOX                  101
#define IDD_SPLASHSCREEN                102
#define IDB_BITMAP_SPLASHSCREEN         104
#define IDC_CHECKSTARTUP                1001
#define IDC_STARTUPMESSAGE              1002
#define IDC_OPENRELEASENOTES            1003

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
