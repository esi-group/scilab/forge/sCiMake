FILE(GLOB
  INC
  includes/*.h
)

DEFINE_INCLUDES(${INC})

FILE(GLOB
  CALLSCILAB_C_SOURCES
  src/c/call_scilab.c  
  src/c/fromc.c  
  src/c/fromjava.c  
  src/c/SendScilabJobs.c
)

FILE(GLOB
  GATEWAY_C_SOURCES 
  sci_gateway/c/sci_fromjava.c  
  sci_gateway/c/sci_fromc.c  
  sci_gateway/c/gw_call_scilab.c
)

ADD_LIBRARY(call_scilab
  ${CALLSCILAB_C_SOURCES}
  ${GATEWAY_C_SOURCES}
)

TARGET_LINK_LIBRARIES(call_scilab
  core
) 
