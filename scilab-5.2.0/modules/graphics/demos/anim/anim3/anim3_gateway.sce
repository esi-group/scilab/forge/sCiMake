//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) INRIA
//
// This file is distributed under the same license as the Scilab package.
//

// =============================================================================
// Demonstrate animation based on the rotation of Lorentz curve
// =============================================================================

//demo_begin();
//demo_message("SCI/modules/graphics/demos/anim/anim3/anim3.sce");
demo_run("SCI/modules/graphics/demos/anim/anim3/anim3.sce");
//demo_end();
