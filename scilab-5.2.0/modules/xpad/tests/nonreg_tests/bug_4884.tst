// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Bernard HUGUENEY
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- INTERACTIVE TEST -->
// <-- TEST WITH XPAD -->
//
// <-- Non-regression test for bug 4884 -->
//
// <-- Bugzilla URL -->
// http://bugzilla.scilab.org/show_bug.cgi?id=4884
//
// <-- Short Description -->
//    Xpad should preserve end of line (\n \r\n or \n)

// for f=["xpad_dos_eol.sci","xpad_macosx_eol.sci","xpad_linux_eol.sci"], xpad("SCI/modules/xpad/tests/nonreg_tests/"+f),end;

// Wait for end of files loading

// Save files

// check that they did not change (same md5sum)



