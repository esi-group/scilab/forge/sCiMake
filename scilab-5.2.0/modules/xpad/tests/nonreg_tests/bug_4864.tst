// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Bernard HUGUENEY
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- INTERACTIVE TEST -->
// <-- TEST WITH XPAD -->
//
// <-- Non-regression test for bug 4864 -->
//
// <-- Bugzilla URL -->
// http://bugzilla.scilab.org/show_bug.cgi?id=4864
//
// <-- Short Description -->
//    Ctrl-z should remove "modifed flag" when undoing the last (in fact first) modification

// xpad(SCI/modules/xpad/tests/nonreg_tests/xpad_indent_test.sci)

// Do some modifications (insert/deletions)

// Undo them with Ctrl-z


// Ensure that the tab name is not prefixed by a "*" anymore






