/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2007 - INRIA - Jean-Baptiste Silvy
 * desc : Contains mapping of java method used by DrawableSubwin
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include "DrawableSubwinJavaMapper.hxx"

extern "C"
{
#include "getScilabJavaVM.h"
}

namespace sciGraphics
{

/*--------------------------------------------------------------------------*/
DrawableSubwinJavaMapper::DrawableSubwinJavaMapper(void)
{
  m_pJavaObject = new org_scilab_modules_renderer_subwinDrawing::DrawableSubwinGL(getScilabJavaVM());
}
/*--------------------------------------------------------------------------*/
DrawableSubwinJavaMapper::~DrawableSubwinJavaMapper(void)
{
  delete m_pJavaObject;
  m_pJavaObject = NULL;
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::display(void)
{
  m_pJavaObject->display();
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::initializeDrawing(int figureIndex)
{
  m_pJavaObject->initializeDrawing(figureIndex);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::endDrawing(void)
{
  m_pJavaObject->endDrawing();
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::show(int figureIndex)
{
  m_pJavaObject->show(figureIndex);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::destroy(int parentFigureIndex)
{
  m_pJavaObject->destroy(parentFigureIndex);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::setSubwinParameters(int subwinIndex, bool is2d)
{
  m_pJavaObject->setSubwinParameters(subwinIndex, is2d);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::interactiveRotation(long subwinIndex)
{
	m_pJavaObject->interactiveRotation(subwinIndex);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::interactiveZoom(long subwinIndex)
{
	m_pJavaObject->interactiveZoom(subwinIndex);
}
/*--------------------------------------------------------------------------*/
void DrawableSubwinJavaMapper::unzoom(long subwinHandle)
{
	m_pJavaObject->unzoom((long long) subwinHandle);
}
/*--------------------------------------------------------------------------*/
}
