/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2007 - INRIA - Sylvestre LEDRU 
 * Copyright (C) 2007 - INRIA - Allan CORNET 
 * Skeleton for the gateway header file (declaration of the "links" 
 * between Scilab and the "library"
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#ifndef __GW_RENDERER__
#define __GW_RENDERER__
/*--------------------------------------------------------------------------*/
#include "dynlib_renderer.h"
/*--------------------------------------------------------------------------*/
int gw_renderer(void);
/*--------------------------------------------------------------------------*/
#endif /*  __GW_RENDERER__ */


