# Localization of the module localization
# Please see in SCI/tools/localization for localization management
# Copyright (C) 2007/2008 - INRIA / Scilab
# This file is distributed under the same license as the Scilab package.
# Sylvestre Ledru <sylvestre.ledru@inria.fr>, 2007/2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Scilab\n"
"Report-Msgid-Bugs-To: <localization@scilab.org>\n"
"POT-Creation-Date: 2007-11-26 18:05+0100\n"
"Last-Translator: Sylvestre Ledru <sylvestre.ledru@inria.fr>\n"
"Language-Team: Scilab Localization <localization@scilab.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Revision-Date: 2009-12-09 16:34+0100\n"

# File: sci_gateway/c/sci_setdefaultlanguage.c, line: 41
#, c-format
msgid "%s: This feature is only used on Windows.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setlanguage.c, line: 48
# File: sci_gateway/c/sci_setlanguage.c, line: 52
#, c-format
msgid "%s: Unsupported language '%s'.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setdefaultlanguage.c, line: 125
#, c-format
msgid "%s: Wrong type for first input argument: String expected.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setlanguage.c, line: 104
# File: sci_gateway/c/sci_gettext.c, line: 131
#, c-format
msgid "%s: Wrong type for input argument #%d: String expected.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setdefaultlanguage.c, line: 106
msgid "Restart Scilab to apply to menus.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setlanguage.c, line: 92
#, c-format
msgid "Switching to default language : '%s'.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setdefaultlanguage.c, line: 105
msgid "The language for menus cannot be changed on a running console.\n"
msgstr ""
#
# File: sci_gateway/c/sci_setlanguage.c, line: 91
# File: sci_gateway/c/sci_setdefaultlanguage.c, line: 62
#, c-format
msgid "Unsupported language '%s'.\n"
msgstr ""
