<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) INRIA - 
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="kpure">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>kpure</refname>
    <refpurpose> continuous SISO system limit feedback gain</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Calling Sequence</title>
    <synopsis>
  K=kpure(sys [,tol])
  [K,R]=kpure(sys [,tol])
</synopsis>
  </refsynopsisdiv>
  <refsection>
    <title>Parameters</title>
    <variablelist>
      <varlistentry>
        <term>sys</term>
        <listitem>
          <para>SISO linear system (syslin)</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>tol</term>
        <listitem>
          <para>vector with 2 elements <literal>[epsK epsI]</literal>. <literal>epsK</literal> is a tolerance used to determine if two values of  <literal>K</literal> can be considered as  equal <literal>epsI</literal> is a tolerance used to determine if a root is imaginary or not. The default value is <literal>[1e-6 1e-6 ]</literal> </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>K</term>
        <listitem>
          <para>Real vector, the vector of gains for which at least one closed loop pole is imaginary. </para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>R</term>
        <listitem>
          <para>Complex vector, the imaginary closed loop poles associated with the  values of  <literal>K</literal>.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>
  <refsection>
    <title>Description</title>
    <para><literal>K=kpure(sys)</literal> computes the gains <literal>K</literal> such that the system
    <literal>sys</literal> feedback by <literal>K(i)</literal> (<literal>sys/.K(i)</literal>) has  poles on imaginary axis.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[ 
s=poly(0,'s');
h=syslin('c',(s-1)/(1+5*s+s^2+s^3))
clf();evans(h)
K=kpure(h)
hf=h/.K(1)
roots(denom(hf))
 ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="evans">evans</link>
      </member>
      <member>
        <link linkend="krac2">krac2</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
