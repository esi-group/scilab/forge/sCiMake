/*-----------------------------------------------------------------------------------*/
/* INRIA 2006 */
/* Allan CORNET */
/*-----------------------------------------------------------------------------------*/ 
#ifndef __GETMAXMALLOC_H__
#define __GETMAXMALLOC_H__
/*-----------------------------------------------------------------------------------*/ 
#include "ExportImport.h" /* IMPORT_EXPORT_MALLOC_DLL */

IMPORT_EXPORT_MALLOC_DLL unsigned long GetLargestFreeMemoryRegion(void);

#endif /* __GETMAXMALLOC_H__ */
/*-----------------------------------------------------------------------------------*/ 

