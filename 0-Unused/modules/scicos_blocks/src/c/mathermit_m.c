/*  Scicos
*
*  Copyright (C) INRIA - METALAU Project <scicos@inria.fr>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*
* See the file ./license.txt
*/
#include "scicos_block4.h"
extern int C2F(mtran)();
void mathermit_m(scicos_block *block,int flag)
{
  double *ur;
  double *yr;
  double *ui;
  double *yi;
  int nu,mu,i;

  nu=GetInPortRows(block,1);
  mu=GetInPortCols(block,1);

  ur=GetRealInPortPtrs(block,1);
  ui=GetImagInPortPtrs(block,1);
  yr=GetRealOutPortPtrs(block,1);
  yi=GetImagOutPortPtrs(block,1);
  C2F(mtran)(ur,&nu,yr,&mu,&nu,&mu);
  C2F(mtran)(ui,&nu,yi,&mu,&nu,&mu);
  for(i=0;i<mu*nu;i++) *(yi+i)=-(*(yi+i));
}

