//
// Scilab ( http://www.scilab.org/ ) 
// This file is part of Scilab
// Copyright (C) 2008 - INRIA
// Copyright (C) 2009 - DIGITEO
//
// This file is distributed under the same license as the Scilab package.
//

exec("SCI/modules/xcos/demos/PENDULUM_ANIM.sci");
exec("SCI/modules/xcos/demos/anim_pen.sci");

xcos("SCI/modules/xcos/demos/pendulum_anim5.xcos");
