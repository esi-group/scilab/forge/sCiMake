package org.scilab.modules.xcos.utils;

import com.mxgraph.util.mxConstants;

public class XcosConstants extends mxConstants{

    public static int PALETTE_BLOCK_WIDTH = 100;
    public static int PALETTE_BLOCK_HEIGHT = 100;

    public static final int PALETTE_HMARGIN = 5;
    public static final int PALETTE_VMARGIN = 5;

    /**
     * Defines the key for flip image .
     */
    public static String STYLE_FLIP = "flip";

    /**
     * Defines the key for mirror image .
     */
    public static String STYLE_MIRROR = "mirror";

}
