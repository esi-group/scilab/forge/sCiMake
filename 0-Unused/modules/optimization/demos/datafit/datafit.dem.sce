//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) ????-2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

mode(-1);

path=get_absolute_file_path('datafit.dem.sce');
exec(path+'demo_datafit.sci');
demo_datafit();
