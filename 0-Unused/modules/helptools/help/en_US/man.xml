<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2008 - INRIA
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:lang="en" xml:id="man">
  <info>
    <pubdate>$LastChangedDate$</pubdate>
  </info>
  <refnamediv>
    <refname>man</refname>
    <refpurpose> on line help XML file description format</refpurpose>
  </refnamediv>
  <refsection>
    <title>Description</title>
    <para>
    The on line help source files are written in XML.
  </para>
    <para>
    Source files (with extension .xml) can be found in the
    <literal>&lt;SCIDIR&gt;/man/&lt;language&gt;/*</literal> directories. The file name is usually
    associated to a keyword (corresponding to a function name most of the cases) it describes.
  </para>
  </refsection>
  <refsection>
    <title>A few words about XML</title>
    <para>An XML file resembles to an HTML file but with both a more rigid and free syntax.
     Free because you may build your own tags: the set of tags together with its rules
     must be described somewhere, generally in another file (<literal>&lt;SCIDIR&gt;/man/manrev.dtd</literal> 
     for scilab),
     and rigid because, once the tags and rules are defined (which are called the Definition
     Type Document: DTD) , you must respect its (in particular to every open tags 
     <literal>&lt;MY_TAG&gt;</literal> must correspond a closed  <literal>&lt;/MY_TAG&gt;</literal>).</para>
    <para> 
     The DTD manrev.dtd is written in SGML and precises the exact syntax required by a 
     scilab XML help page. So if you know this language you may red this file. 
     The following annotated example (see the next section) shows you some possibilities 
     offered by this DTD and may be enough to write simple help pages.
     </para>
    <para>
     Once an XML page is written and conforms to the DTD, it may be transformed in
     HTML to be red by your favorite browser or by the tcltk scilab browser
     (see section browser choice in this page). The XML -&gt; HTML translation is controled
     by a set of rules written in the (XML) file <literal>&lt;SCIDIR&gt;/man/language/html.xsl</literal>.
     Those rules are currently more or less restricted to fit the tcltk scilab browser
     features (which may display correctly only basic HTML): if you use a real HTML
     browser and want a better appearance you have to modify this file.
     </para>
  </refsection>
  <refsection>
    <title>How to write a simple xml scilab help page: the lazzy way</title>
    <para>
         If one want to write the xml file associated to a new scilab function
	he or she may use the Scilab function <link linkend="help_skeleton">help_skeleton</link> to produce
	the skeleton of the xml file. In most cases the user will not be
	required to know xml syntax. 
       </para>
  </refsection>
  <refsection>
    <title>How to write a simple xml scilab help page: an example</title>
    <para>
     Here is a simple annotated XML scilab help page which describes an hypothetic foo scilab
     function. In the following, the XML file is displayed in a <literal>type writer font</literal>
     and cut-out in several parts, each part being preceded by some associated explanations.
     The entire XML file <literal>foo.xml</literal> is in the <literal>&lt;SCIDIR&gt;/man/eng/utility</literal>
     directory and the result may be displayed by clicking on foo.
     (you may found others examples in the <literal>&lt;SCIDIR&gt;/examples/man-examples-xml</literal> 
     directory). <emphasis role="bold">Finally</emphasis> note that some tag pairs <literal>&lt;TAG&gt;</literal>, 
     <literal>&lt;/TAG&gt;</literal> have been renamed here <literal>&lt;ATAG&gt;</literal>, 
     <literal>&lt;/ATAG&gt;</literal>. This is because some scilab scripts which do some work
     on or from the xml files don't verify if a tag is inside a <literal>VERBATIM</literal> entry.
     </para>
    <para>
      The 3 first lines of the file are mandatory, the second precises the path to the
      DTD file and the third, formed by the <literal>&lt;MAN&gt;</literal> tag, begin the 
      hierarchical description (the file must finish with the <literal>&lt;/MAN&gt;</literal> tag). 
      The 4 followings entries : <literal>LANGUAGE</literal>, <literal>TITLE</literal>, 
      <literal>TYPE</literal> and  <literal>DATE</literal>, are also mandatory (in this order) the text 
      corresponding to  <literal>&lt;TYPE&gt;</literal> being generally 'Scilab function' 
      (most of the cases) but may be simply  'Scilab keyword' or 'Scilab data type', ..., 
      depending of what explains the help page. 
   </para>
    <programlisting><![CDATA[ 
<!DOCTYPE MAN SYSTEM "<SCIDIR>/man/manrev.dtd">
<MAN>
   <LANGUAGE>eng</LANGUAGE>
   <TITLE>foo</TITLE>
   <TYPE>Scilab function</TYPE>
   <DATE>$LastChangedDate$</DATE>
 ]]></programlisting>
    <para>
   The first of these 2 following entries (<literal>SHORT_DESCRIPTION</literal>)
   is mandatory and important since the words of the short description text, are used by the
   <link linkend="apropos">apropos</link> command to search help pages from a keyword: the short description
   is used to build the <literal>whatis.html</literal> file corresponding to your toolbox and 
   the <literal>apropos keyword</literal> command looks in all the whatis files and then proposes
   the links to every page containing the word <literal>keyword</literal> in its  short description
   (in fact the actual associated tags are <literal>&lt;SHORT_DESCRIPTION&gt;</literal> and 
   <literal>&lt;/SHORT_DESCRIPTION&gt;</literal> and not <literal>&lt;ASHORT_DESCRIPTION&gt;</literal>
   and <literal>&lt;/ASHORT_DESCRIPTION&gt;</literal>).
   The next entry (<literal>CALLING_SEQUENCE</literal>) must be used if you describe a function 
   (but is not strictly mandatory). If your function have several calling sequences
   use several <literal>CALLING_SEQUENCE_ITEM</literal> entries.
   </para>
    <programlisting><![CDATA[

          <ASHORT_DESCRIPTION name="foo">foo short description</ASHORT_DESCRIPTION>
          <CALLING_SEQUENCE>
              <CALLING_SEQUENCE_ITEM>[y] = foo(x)</CALLING_SEQUENCE_ITEM>
          </CALLING_SEQUENCE>
   
        ]]></programlisting>
    <para>
   The following entry (<literal>PARAM</literal>) is not strictly mandatory but is
   the good one to describe each parameters (input and output) in case of a function.
   </para>
    <programlisting><![CDATA[

          <PARAM>
             <PARAM_INDENT>
                <PARAM_ITEM>
                   <PARAM_NAME>x</PARAM_NAME>
                   <PARAM_DESCRIPTION>
                      <SP>: what may be x</SP>
                   </PARAM_DESCRIPTION> 
                </PARAM_ITEM>
                <PARAM_ITEM>
                   <PARAM_NAME>y</PARAM_NAME>
                   <PARAM_DESCRIPTION>
                      <SP>: what may be y</SP>
                   </PARAM_DESCRIPTION> 
                </PARAM_ITEM>
             </PARAM_INDENT>
          </PARAM>
   
        ]]></programlisting>
    <para>
   The <literal>DESCRIPTION</literal> entry is perhaps the most significant one (but not strictly 
   mandatory) and may be more sophisticated than in this example (for instance you may have    
   <literal>DESCRIPTION_ITEM</literal> sub-entries). Here you see how to write several paragraphes
   (each one enclosed between the <literal>&lt;P&gt;</literal> and <literal>&lt;/P&gt;</literal> tags),
   how to emphasis a variable or a function name (by enclosing it between the 
   <literal>&lt;VERB&gt;</literal> and <literal>&lt;/VERB&gt;</literal> tags), how to
   emphasis a part of text (<literal>&lt;EM&gt;</literal> or <literal>&lt;BD&gt;</literal>
   and <literal>&lt;TT&gt;</literal> to put it in a type writer font)), and finally, how to put a link onto 
   another help page  (in fact the actual associated tags are <literal>&lt;LINK&gt;</literal> and 
   <literal>&lt;/LINK&gt;</literal> and not <literal>&lt;ALINK&gt;</literal>
   and <literal>&lt;/ALINK&gt;</literal>).
   </para>
    <programlisting><![CDATA[

         <DESCRIPTION>
             <P>
                A first paragraph which explains what computes the foo function.
                If you want to emphasis a parameter name then you use the following
                tag <VERB>x</VERB>, if you want to emphasis a part of text
                <EM>inclose it inside theses tags</EM> and use theses ones
                <BD>to have a bold font</BD> and finally <TT>for a type writer style</TT>.
             </P>
             <P>
                A second paragraph... Here is an example of a link to another page :
                <ALINK>man</ALINK>.
             </P>
         </DESCRIPTION>
   
        ]]></programlisting>
    <para>
  Here is how to write your own entry, for instance to describe some outside remarks
  and/or notes about your wonderful function.  
  </para>
    <programlisting><![CDATA[

         <SECTION label='Notes'>
             <P>
                Here is a list of notes :
             </P>
             <ITEM label='first'><SP>blablabla...</SP></ITEM>
             <ITEM label='second'><SP>toto is the french foo...</SP></ITEM>
         </SECTION>
   
        ]]></programlisting>
    <para>
  An important entry is the <literal>EXAMPLE</literal> one which is reserved to show scilab
  uses of your function (begin with simple ones !). Note that you must close this entry
  with  <literal>]]&gt;&lt;/EXAMPLE&gt;</literal> and not like here with <literal>}}&gt;&lt;/EXAMPLE&gt;</literal>
  (once again this is a bad trick to avoid some interpretation problems).</para>
    <programlisting><![CDATA[

         <EXAMPLE><![CDATA[
             deff("y=foo(x)","y=x"); // define the foo function as the identity function
             foo("toto")
      }}></EXAMPLE>
   
        ]]></programlisting>
    <para>
  This last part explains how to put the links onto others related help pages 
  (as said before the good tags are in fact <literal>&lt;LINK&gt;</literal> and 
  <literal>&lt;/LINK&gt;</literal> and not <literal>&lt;ALINK&gt;</literal> and 
  <literal>&lt;/ALINK&gt;</literal> ) and finally how to reveal your name if you want 
  (use one <literal>AUTHOR_ITEM</literal> entry by author). Perhaps it is a good idea 
  to put an email adress if you look for bug reports !</para>
    <programlisting><![CDATA[

         <SEE_ALSO>
           <SEE_ALSO_ITEM> <ALINK>man</ALINK> </SEE_ALSO_ITEM>
           <SEE_ALSO_ITEM> <ALINK>apropos</ALINK> </SEE_ALSO_ITEM>
         </SEE_ALSO>

         <AUTHOR>
           <AUTHOR_ITEM>B. P.</AUTHOR_ITEM>
         </AUTHOR>
       </MAN>
   
        ]]></programlisting>
  </refsection>
  <refsection>
    <title>How to create an help chapter</title>
    <para> Create a directory and write down a set of xml files build as described
    above. Then start Scilab and execute <literal>xmltohtml(dir)</literal>, where
      <literal>dir</literal> is a character string giving the path of the directory
      (see <link linkend="xmltohtml">xmltohtml</link> for more details) .</para>
  </refsection>
  <refsection>
    <title>How to make Scilab know a new help chapter</title>
    <para>This can be done by the function <link linkend="add_help_chapter">add_help_chapter</link>.</para>
  </refsection>
  <refsection>
    <title>Examples</title>
    <programlisting role="example"><![CDATA[
    function y=foo(a,b,c),y=a+2*b+c,endfunction
    path=help_skeleton('foo',TMPDIR)
    if (isdef('editor') | (funptr('editor')<>0)) then
      editor(path);
    end
  ]]></programlisting>
  </refsection>
  <refsection>
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="apropos">apropos</link>
      </member>
      <member>
        <link linkend="help">help</link>
      </member>
      <member>
        <link linkend="help_skeleton">help_skeleton</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
