// =============================================================================
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2009 - DIGITEO - Pierre MARECHAL <pierre.marechal@scilab.org>
//
//  This file is distributed under the same license as the Scilab package.
// =============================================================================

// <-- JVM NOT MANDATORY -->

load("SCI/modules/atoms/macros/atoms_internals/lib");

// Load the 1st scenario : See scene1.test.atoms.scilab.org.txt
atomsRepositorySetOfl("http://scene1.test.atoms.scilab.org");

// Do not use the autoload system
config_autoload = atomsGetConfig("autoloadAddAfterInstall");
config_Verbose  = atomsGetConfig("Verbose");
atomsSetConfig("autoloadAddAfterInstall","False");
atomsSetConfig("Verbose" ,"False");

// Install the toolbox 5
// =============================================================================

atomsInstall("toolbox_5");

// Check if the module is really installed
if ~atomsIsInstalled("toolbox_5") then pause, end
if ~atomsIsInstalled("toolbox_4") then pause, end
if ~atomsIsInstalled("toolbox_2") then pause, end
if ~atomsIsInstalled("toolbox_1") then pause, end

// Remove the module
atomsRemove("toolbox_5");

// Install the toolbox 5 (user section)
// =============================================================================

atomsInstall("toolbox_5","user");

if ~atomsIsInstalled("toolbox_5","user") then pause, end
if ~atomsIsInstalled("toolbox_4","user") then pause, end
if ~atomsIsInstalled("toolbox_2","user") then pause, end
if ~atomsIsInstalled("toolbox_1","user") then pause, end

if atomsIsInstalled("toolbox_5","allusers") then pause, end
if atomsIsInstalled("toolbox_4","allusers") then pause, end
if atomsIsInstalled("toolbox_2","allusers") then pause, end
if atomsIsInstalled("toolbox_1","allusers") then pause, end

atomsRemove("toolbox_5","user");

if atomsIsInstalled("toolbox_5","user") then pause, end
if atomsIsInstalled("toolbox_4","user") then pause, end
if atomsIsInstalled("toolbox_2","user") then pause, end
if atomsIsInstalled("toolbox_1","user") then pause, end

// Install the toolbox 5 (allusers section)
// =============================================================================

atomsInstall("toolbox_5","allusers");

if ~atomsIsInstalled("toolbox_5","allusers") then pause, end
if ~atomsIsInstalled("toolbox_4","allusers") then pause, end
if ~atomsIsInstalled("toolbox_2","allusers") then pause, end
if ~atomsIsInstalled("toolbox_1","allusers") then pause, end

if atomsIsInstalled("toolbox_5","user") then pause, end
if atomsIsInstalled("toolbox_4","user") then pause, end
if atomsIsInstalled("toolbox_2","user") then pause, end
if atomsIsInstalled("toolbox_1","user") then pause, end

atomsRemove("toolbox_5","allusers");

if atomsIsInstalled("toolbox_5","allusers") then pause, end
if atomsIsInstalled("toolbox_4","allusers") then pause, end
if atomsIsInstalled("toolbox_2","allusers") then pause, end
if atomsIsInstalled("toolbox_1","allusers") then pause, end

// Install the toolbox 5 (Both section)
// =============================================================================

atomsInstall("toolbox_5","allusers");
atomsInstall("toolbox_5","user");

if ~atomsIsInstalled("toolbox_5","allusers") then pause, end
if ~atomsIsInstalled("toolbox_4","allusers") then pause, end
if ~atomsIsInstalled("toolbox_2","allusers") then pause, end
if ~atomsIsInstalled("toolbox_1","allusers") then pause, end

if ~atomsIsInstalled("toolbox_5","user") then pause, end
if ~atomsIsInstalled("toolbox_4","user") then pause, end
if ~atomsIsInstalled("toolbox_2","user") then pause, end
if ~atomsIsInstalled("toolbox_1","user") then pause, end

atomsRemove("toolbox_5","allusers");
if atomsIsInstalled("toolbox_5","allusers") then pause, end
if atomsIsInstalled("toolbox_4","allusers") then pause, end
if atomsIsInstalled("toolbox_2","allusers") then pause, end
if atomsIsInstalled("toolbox_1","allusers") then pause, end

if ~atomsIsInstalled("toolbox_5","user") then pause, end
if ~atomsIsInstalled("toolbox_4","user") then pause, end
if ~atomsIsInstalled("toolbox_2","user") then pause, end
if ~atomsIsInstalled("toolbox_1","user") then pause, end

atomsInstall("toolbox_5","allusers");
atomsRemove("toolbox_5","user");

if atomsIsInstalled("toolbox_5","user") then pause, end
if atomsIsInstalled("toolbox_4","user") then pause, end
if atomsIsInstalled("toolbox_2","user") then pause, end
if atomsIsInstalled("toolbox_1","user") then pause, end

if ~atomsIsInstalled("toolbox_5","allusers") then pause, end
if ~atomsIsInstalled("toolbox_4","allusers") then pause, end
if ~atomsIsInstalled("toolbox_2","allusers") then pause, end
if ~atomsIsInstalled("toolbox_1","allusers") then pause, end

atomsRemove("toolbox_5","allusers");

// Restore original values
// =============================================================================
atomsSetConfig("autoloadAddAfterInstall",config_autoload);
atomsSetConfig("Verbose" ,config_Verbose);
atomsRepositorySetOfl("http://atoms.scilab.org");
