<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="EVTDLY_c">
  <refnamediv>
    <refname>EVTDLY_c</refname>
    <refpurpose>Retard événementiel</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/EVTDLY_c_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_EVTDLY_c">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="EVTDLY_c">Retard événementiel</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Description_EVTDLY_c">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_EVTDLY_c">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_EVTDLY_c">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_EVTDLY_c">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_EVTDLY_c">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_EVTDLY_c">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_EVTDLY_c">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_EVTDLY_c">
    <title>Description</title>
    <para>
Chaque événement de sortie est réalisé après l'événement 
d'entrée par un laps de temps <emphasis role="bold">Delay</emphasis>.
</para>
    <para>
La date de l'événement de sortie est donc déterminée par la formule :
</para>
    <para>
</para>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/EVTDLY_c_img2_fr.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_EVTDLY_c">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/EVTDLY_c_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Delay</emphasis>
        </para>
        <para> Un scalaire à valeur réelle donnant le retard temporel entre les événements d'entrée et de sortie.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Date of initial output event</emphasis>
        </para>
        <para> Un scalaire à valeur réelle donnant la première date de l'événement de sortie. Si aucune date initiale est nécéssaire alors ce champ doit être renseigné par une valeur négative.</para>
        <para> Propriétés : Type 'vec' de taille 1</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_EVTDLY_c">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">evtdly4</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_EVTDLY_c">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Events/EVTDLY_c.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_EVTDLY_c">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/evtdly4.c (Type 4)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_EVTDLY_c">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="CLOCK_c">CLOCK_c - Horloge d'activation (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_EVTDLY_c">
    <title>Auteurs</title>
    <para><emphasis role="bold">Alan Layec</emphasis> - INRIA</para>
  </refsection>
</refentry>
