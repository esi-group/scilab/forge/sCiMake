<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="LOGIC">
  <refnamediv>
    <refname>LOGIC</refname>
    <refpurpose>Logique combinatoire</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/LOGIC_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_LOGIC">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="LOGIC">Logique combinatoire</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_LOGIC">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_LOGIC">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_LOGIC">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_LOGIC">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_LOGIC">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_LOGIC">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_LOGIC">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_LOGIC">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_LOGIC">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Integer_pal">Integer - Palette Integer</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_LOGIC">
    <title>Description</title>
    <para>
This block implements a standard truth table for modeling programming array, digital circuit and any other boolean expressions. The user can specify a matrix that defines all the possible block output in the Truth table field. Each row of the matrix contains the output of different combination of input elements. The number of rows must be a power of two, it defines the number of inputs using the equation:  
</para>
    <para>
number of row = 2 (number of input) 
</para>
    <para>
The number of outputs is equal to the number of columns of the matrix.  
</para>
    <para>
This block support only the int8 data type. When the input is positif, the input is considered as logical 1, When it is negatif or zero it is considered as logical 0.  
</para>
    <para>
This block can be activated by an implicit input event or it can herit the clock from the regular input.  
</para>
    <para>
This block is used to implement SR and JK flip-flops.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_LOGIC">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/LOGIC_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Truth table</emphasis>
        </para>
        <para> The matrix of outputs. For more information see the description part.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-2]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Inherit(0=no 1=yes)</emphasis>
        </para>
        <para> Specifies if the clock is inherit or not.</para>
        <para> Propriétés : Type 'vec' de taille 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_LOGIC">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 5</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : taille [1,1] / type 5</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 5</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">logic</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_LOGIC">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/IntegerOp/LOGIC.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_LOGIC">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/logic.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_LOGIC">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="SRFLIPFLOP">SRFLIPFLOP - SRFLIPFLOP Bascule RS (Bloc Scicos)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="JKFLIPFLOP">JKFLIPFLOP - JKFLIPFLOP Bascule JK (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_LOGIC">
    <title>Auteurs</title>
    <para><emphasis role="bold">Fady NASSIF</emphasis> - INRIA</para>
  </refsection>
</refentry>
