<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="INTEGRAL_m">
  <refnamediv>
    <refname>INTEGRAL_m</refname>
    <refpurpose>Integration</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/INTEGRAL_m_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_INTEGRAL_m">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="INTEGRAL_m">Integration</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_INTEGRAL_m">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_INTEGRAL_m">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_INTEGRAL_m">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_INTEGRAL_m">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_INTEGRAL_m">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_INTEGRAL_m">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_INTEGRAL_m">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_INTEGRAL_m">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Linear_pal">Linear - Palette Lineaire</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_INTEGRAL_m">
    <title>Description</title>
    <para>
Ce bloc est un intégrateur. La sortie est l'intégrale de l'entrée.

</para>
  </refsection>
  <refsection id="Boîtededialogue_INTEGRAL_m">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/INTEGRAL_m_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Initial Condition</emphasis>
        </para>
        <para> Un vecteur ou un scalaire définissant les conditions initiales.</para>
        <para> La définition du type de donnée en entrée/sortie est définit grâce à ce paramètre. Le type peut être réel ou complexe.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">With re-intialization</emphasis>
        </para>
        <para> Si ce paramètre est mis à<emphasis role="bold">1</emphasis> , celui-ci permet la réinitialisation de l'état à une condition initiale définie par la valeur présente sur le deuxième port d'entrée régulier.</para>
        <para> Propriétés : Type 'vec' de taille 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">With saturation</emphasis>
        </para>
        <para> Si ce paramètre est placé à 1, alors les états sont limités à des valeurs qui seront comprises entre la limite basse de saturation (Lower limit) et la limite haute de saturation (Upper limit).</para>
        <para> Propriétés : Type 'vec' de taille 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Upper limit</emphasis>
        </para>
        <para> La limite haute de saturation de l'intégration.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Lower limit</emphasis>
        </para>
        <para> La limite basse de saturation de l'intégration.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1].</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_INTEGRAL_m">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">integral_func</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_INTEGRAL_m">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Linear/INTEGRAL_m.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_INTEGRAL_m">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/integral_func.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/integralz_func.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_INTEGRAL_m">
    <title>Auteurs</title>
    <para>
      <emphasis role="bold"/>
    </para>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Fady NASSIF</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Alan Layec</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Ramine Nikoukhah</emphasis> INRIA</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
