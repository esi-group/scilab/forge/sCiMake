<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="TCLSS">
  <refnamediv>
    <refname>TCLSS</refname>
    <refpurpose>Système linéaire continu avec saut</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/TCLSS_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_TCLSS">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="TCLSS">Système linéaire continu avec saut</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_TCLSS">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_TCLSS">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_TCLSS">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_TCLSS">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_TCLSS">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_TCLSS">Fonction de calcul</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_TCLSS">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Linear_pal">Linear - Palette Lineaire</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_TCLSS">
    <title>Description</title>
    <para>
Ce bloc réalise un système d'équations d'etat en temporel continu avec
la possibilités de réaliser des sauts dans l'état.
Le nombre d'entrées de ce bloc est deux.
La première entrée est l'entrée régulière du système linéaire.
La seconde fournie la valeur du nouvel état qui est copié dans l'état du système lorsque le bloc est activé par un événement sur son unique port d'entrée événementiel.
Cela signifique que l'état du système saute à la valeur présente sur le deuxième port d'entrée (de taille égale à l'état du système).
Le système est défini par les matrices 
et par l'état initial .
Les dimensions des matrices et de l'état initial doivent être appropriées.
Les tailles des entrées et des sorties sont ajustées automatiquement.

</para>
  </refsection>
  <refsection id="Boîtededialogue_TCLSS">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/TCLSS_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold"><inlinemediaobject><imageobject><imagedata fileref="../../../images/TCLSS_img5_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> matrix</emphasis>
        </para>
        <para> Une matrice carré.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">B matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/TCLSS_img6_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas d'entrées.</para>
        <para> Propriétés : Type 'mat' de taille ["size(%1,2)","-1"]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">C matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/TCLSS_img7_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas de sorties.</para>
        <para> Propriétés : Type 'mat' de taille ["-1","size(%1,2)"]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">D matrix</emphasis>
        </para>
        <para> La matrice<inlinemediaobject><imageobject><imagedata fileref="../../../images/TCLSS_img8_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> , [] si le système n'a pas de terme D.</para>
        <para> Propriétés : Type 'mat' de taille [-1,-1]. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Initial state</emphasis>
        </para>
        <para> Un état initial du système ( vectoriel ou scalaire).</para>
        <para> Propriétés : Type 'vec' de taille "size(%1,2)". </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_TCLSS">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">sorties régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> oui</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">tcslti4</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_TCLSS">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Linear/TCLSS.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_TCLSS">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/tcslti4.c (Type 4)</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
