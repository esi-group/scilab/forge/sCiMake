<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="PerteDP">
  <refnamediv>
    <refname>PerteDP</refname>
    <refpurpose>Perte de charge thermohydraulique (Tuyau)</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/PerteDP_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_PerteDP">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="PerteDP">Perte de charge thermohydraulique (Tuyau)</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_PerteDP">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_PerteDP">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_PerteDP">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_PerteDP">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_PerteDP">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <link linkend="PerteDP">Modèle Modelica</link>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_PerteDP">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="ThermoHydraulics_pal">ThermoHydraulics.cosf - Boîte à outils des composants thermohydrauliques</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_PerteDP">
    <title>Description</title>
    <para>
Un bloc de PertDP représente un tuyau hydraulique avec des pertes de
charge. Ce composant représente une perte de charge hydraulique où la
perte de pression est directement proportionnelle au débit, état
laminaire.  Ce bloc a une direction; c.-à-d. la direction positive est
quand le fluide entre par le port d'entrée (le noir). Ceci signifie
que ce bloc suppose que le débit est positif si le fluide découle du
port noir vers le port blanc.  Les paramètres importants de ce composant
sont la longueur, le diamètre du tuyau, les altitudes des ports
d'entrée et de sortie, et quelques autres coefficients
thermo-hydrauliques.

</para>
  </refsection>
  <refsection id="Boîtededialogue_PerteDP">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/PerteDP_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Longueur du tube : L (m)</emphasis>
        </para>
        <para> Longeur du tube</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Diamètre interne du tube : D (m)</emphasis>
        </para>
        <para> Diamètre interne du tube</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Coefficient de perte de charge-frottement(S.U) : lambda</emphasis>
        </para>
        <para> Coefficient de perte de charge-frottement(S.U)</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Altitude entrée tuyauterie : z1 (m)</emphasis>
        </para>
        <para> Altitude entrée tuyauterie (z1)</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Altitude sortie tuyauterie : z2 (m)</emphasis>
        </para>
        <para> Altitude entrée tuyauterie (z2)</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Si 0, masse volumique imposée fu fluide : p_rho (kg/m3)</emphasis>
        </para>
        <para> masse volumique imposée fu fluide</para>
        <para> Propriétés : Type 'vec' de taille -1</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_PerteDP">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Entrées :</emphasis>
        </para>
        <itemizedlist>
          <listitem>
            <para><emphasis role="bold">Nom de la variable Modelica :</emphasis> 'C1'
</para>
            <para><emphasis role="bold">Variable</emphasis> implicite.


</para>
          </listitem>
        </itemizedlist>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Sorties :</emphasis>
        </para>
        <itemizedlist>
          <listitem>
            <para><emphasis role="bold">Nom de la variable Modelica :</emphasis> 'C2'
</para>
            <para><emphasis role="bold">Variable</emphasis> implicite.


</para>
          </listitem>
        </itemizedlist>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Paramètres :</emphasis>
        </para>
        <itemizedlist>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'L'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 10
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'D'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 0.2
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'lambda'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 0.03
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'z1'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 0
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'z2'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 0
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
          <listitem>
            <para><emphasis role="bold">Nom du paramètre Modelica :</emphasis> 'p_rho'
</para>
            <para><emphasis role="bold">Valeur par défaut :</emphasis> 0
</para>
            <para><emphasis role="bold">Variable d'état :</emphasis> non.


</para>
          </listitem>
        </itemizedlist>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Nom du fichier model :</emphasis> PerteDP</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_PerteDP">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Hydraulics/PerteDP.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="ModèleModelica_PerteDP">
    <title>Modèle Modelica</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Hydraulics/PerteDP.mo</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
