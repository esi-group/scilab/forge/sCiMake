<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="CSCOPXY">
  <refnamediv>
    <refname>CSCOPXY</refname>
    <refpurpose>Scope y=f(x)</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/CSCOPXY_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contenu_CSCOPXY">
    <title>Contenu</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="CSCOPXY">Scope y=f(x)</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_CSCOPXY">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_CSCOPXY">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Boîtededialogue_CSCOPXY">Boîte de dialogue</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Propriétéspardéfaut_CSCOPXY">Propriétés par défaut</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondinterface_CSCOPXY">Fonction d'interface</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Fonctiondecalcul_CSCOPXY">Fonction de calcul</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Voiraussi_CSCOPXY">Voir aussi</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Auteurs_CSCOPXY">Auteurs</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_CSCOPXY">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Sinks_pal">Sinks - Palette Affichage</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_CSCOPXY">
    <title>Description</title>
    <para>
This block realizes the visualization of the evolution of the two regular inputs signals by drawing the second input as a function of the first at instants of events on the event input port. When a point is drawn on screen it stays until the simulation is finished.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Boîtededialogue_CSCOPXY">
    <title>Boîte de dialogue</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/CSCOPXY_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Number of Curves </emphasis>
        </para>
        <para> Set the number of curves. Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">color </emphasis>
        </para>
        <para> an integer. It is the color number (<inlinemediaobject><imageobject><imagedata fileref="../../../images/CSCOPXY_img3_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> ) or dash type (<inlinemediaobject><imageobject><imagedata fileref="../../../images/CSCOPXY_img4_fr.gif" align="center" valign="middle"/></imageobject></inlinemediaobject> ) used to draw the evolution of the input port signal. See<emphasis role="bold">plot2d</emphasis> for color (dash type) definitions.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">line or mark size</emphasis>
        </para>
        <para> an integer.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Output window number</emphasis>
        </para>
        <para> The number of graphic window used for the display. It is often good to use high values to avoid conflict with palettes and Super Block windows. If you have more than one scope, make sure they don't have the same window numbers (unless superposition of the curves is desired).</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Output window position</emphasis>
        </para>
        <para> a 2 vector specifying the coordinates of the upper left corner of the graphic window. Answer [] for default window position.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Output window sizes</emphasis>
        </para>
        <para> a 2 vector specifying the width and height of the graphic window. Answer [] for default window dimensions.</para>
        <para> Propriétés : Type 'vec' de taille -1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Xmin</emphasis>
        </para>
        <para> Minimum values of the first input; used to set up the X-axis of the plot in the graphics window.</para>
        <para> Propriétés : Type '' de taille </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Xmax</emphasis>
        </para>
        <para> Maximum values of the first input; used to set up the X-axis of the plot in the graphics window.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Ymin</emphasis>
        </para>
        <para> Minimum values of the second input; used to set up the Y-axis of the plot in the graphics window.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Ymax</emphasis>
        </para>
        <para> Maximum values of the second input; used to set up the Y-axis of the plot in the graphics window.</para>
        <para> Propriétés : Type 'vec' de taille 1 </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Buffer size</emphasis>
        </para>
        <para> To improve efficiency it is possible to buffer the input data. The drawing is only done after each Buffer size call to the block.</para>
        <para> Propriétés : Type 'vec' de taille 1</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Propriétéspardéfaut_CSCOPXY">
    <title>Propriétés par défaut</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">toujours actif:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">détection de passage à zéro:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">entrée régulières:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : taille [1,1] / type 1</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : taille [1,1] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des entrées évènementielles:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">nombre des sorties évènementielles:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état continu:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état discret:</emphasis> non</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">possède un état objet:</emphasis> non</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">nom de la fonction de calcul:</emphasis>
          <emphasis role="italic">cscopxy</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondinterface_CSCOPXY">
    <title>Fonction d'interface</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Sinks/CSCOPXY.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Fonctiondecalcul_CSCOPXY">
    <title>Fonction de calcul</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/cscopxy.c (Type 4)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_CSCOPXY">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="CSCOPXY3D">CSCOPXY3D - Scope z=f(x,y) (Bloc Scicos)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_CSCOPXY">
    <title>Auteurs</title>
    <para>
      <emphasis role="bold"/>
    </para>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Ramine Nikoukhah</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Benoit Bayol</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
