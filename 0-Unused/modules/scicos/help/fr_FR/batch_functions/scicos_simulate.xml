<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="scicos_simulate">
  <refnamediv>
    <refname>scicos_simulate</refname>
    <refpurpose>Fonction pour éxécuter une simulation scicos en ligne de commmande</refpurpose>
  </refnamediv>
  <refsynopsisdiv>
    <title>Séquence d'appel</title>
    <synopsis>Info=scicos_simulate(scs_m,Info[,%scicos_context][,flag])</synopsis>
  </refsynopsisdiv>
  <refsection id="Module_scicos_simulate">
    <title>Module</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="scicos_manual">Scicos</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_scicos_simulate">
    <title>Description</title>
    <para>
This function is used to simulate scicos diagrams in batch mode. It requires the scs_m structure which can be obtained by loading in Scilab the .cos file (e.g. load mydiagram.cos).  
</para>
    <para>
Contrary to the function , the diagram need not be compiled before being saved.  
</para>
    <para>
</para>
    <programlisting role="example"><![CDATA[ 
Info=scicos_simulate(scs_m,Info[,%scicos_context][,flag])
 ]]></programlisting>
  </refsection>
  <refsection id="Paramètres_scicos_simulate">
    <title>Paramètres</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">scs_m :</emphasis> scicos diagram (obtained by "load file.cos"). Note that the version of file.cos must be the current version. If not, load into scicos and save.</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Info :</emphasis> a list. It must be list() at the first call, then use output Info as input Info for the next calls. Info contains compilation and simulation information and is used to avoid recompilation when not needed.</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">%scicos_context :</emphasis> a scilab struct containing values of symbolic variables used in the context and Scicos blocks. This is often used to change a parameter in the diagram context. In that case, make sure that in the diagram context the variable is defined such that it can be modified. Say a variable "a" is to be defined in the context having value 1, and later in batch mode, we want to change the value of "a". In that case, in the context of the diagram place: if exists('a') then a=1,end If you want then to run the simulation in batch mode using the value a=2, set: %scicos_context.a=2</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">flag :</emphasis> string. If it equals 'nw' (no window), then blocks using graphical windows are not executed. Note that the list of such blocks must be updated as new blocks are added.</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Info :</emphasis> contains compilation and simulation information and is used to avoid recompilation when not needed. Normally it should not be modified.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Contenudufichier_scicos_simulate">
    <title>Contenu du fichier</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos/macros/scicos_auto/scicos_simulate.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Voiraussi_scicos_simulate">
    <title>Voir aussi</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="scicosim">scicosim - Fonction de simulation scicos en ligne de commande (Fonction Scilab)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="scicos">scicos - Editeur de schéma bloc et interface graphique pour le simulateur hybride (Fonction Scilab)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="steadycos">steadycos - Trouve un régime établi d'un système dynamique décrit par un diagramme scicos (Fonction Scilab)</link>
        </para>
      </listitem>
      <listitem>
        <para>
          <link linkend="lincos">lincos - Construit par linéarisation un modèle linéaire d'équations d'état à partir d'un système dynamique décrit par un diagramme scicos (Fonction Scilab)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Auteurs_scicos_simulate">
    <title>Auteurs</title>
    <para><emphasis role="bold">Ramine Nikoukhah</emphasis> - INRIA</para>
  </refsection>
</refentry>
