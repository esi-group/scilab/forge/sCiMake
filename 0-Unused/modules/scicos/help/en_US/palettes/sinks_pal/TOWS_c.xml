<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="TOWS_c">
  <refnamediv>
    <refname>TOWS_c</refname>
    <refpurpose>Data to Scilab worspace</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/TOWS_c_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_TOWS_c">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="TOWS_c">Data to Scilab worspace</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_TOWS_c">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_TOWS_c">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_TOWS_c">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_TOWS_c">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_TOWS_c">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_TOWS_c">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Seealso_TOWS_c">See also</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Authors_TOWS_c">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_TOWS_c">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Sinks_pal">Sinks - Sinks palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_TOWS_c">
    <title>Description</title>
    <para>
That block is used to put simulated data in the scilab
workspace.
</para>
    <para>
Each sampling time, both dates and values of input are recorded.

</para>
  </refsection>
  <refsection id="Dialogbox_TOWS_c">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/TOWS_c_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Size of buffer</emphasis>
        </para>
        <para> Set the size of the input buffer. That gives the total number of samples recorded during the simulation.</para>
        <para> That buffer is a circulate buffer.</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Scilab variable name</emphasis>
        </para>
        <para> Set the name of the Scilab variable. This must be a valid variable name.</para>
        <para> The simulation must be finished to retrieve that variable in the Scilab workspace.</para>
        <para> Properties : Type 'str' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Inherit (no:0, yes:1)</emphasis>
        </para>
        <para> Options to choose event inheritance from regular input or from explicit event input (0).</para>
        <para> Properties : Type 'vec' of size 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_TOWS_c">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,1] / type -1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 1</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>
          <emphasis role="italic">tows_c</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_TOWS_c">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Sinks/TOWS_c.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_TOWS_c">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/tows_c.c (Type 4)</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Seealso_TOWS_c">
    <title>See also</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="FROMWS_c">FROMWS_c - Data from Scilab worspace to Scicos (Scicos Block)</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Authors_TOWS_c">
    <title>Authors</title>
    <para><emphasis role="bold">Alan Layec</emphasis> - INRIA</para>
  </refsection>
</refentry>
