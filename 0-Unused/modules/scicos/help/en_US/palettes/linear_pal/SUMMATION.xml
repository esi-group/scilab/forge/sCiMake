<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scicos
 * 
 * Copyright (C) INRIA - METALAU Project <scicos@inria.fr> (HTML version)
 * Copyright (C) DIGITEO - Scilab Consortium (XML Docbook version)
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 * 
 * See the file ./license.txt
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" version="5.0-subset Scilab" xml:id="SUMMATION">
  <refnamediv>
    <refname>SUMMATION</refname>
    <refpurpose>SUMMATION Matrix Summation</refpurpose>
  </refnamediv>
  <refsection>
    <title>Block Screenshot</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/SUMMATION_blk.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
  </refsection>
  <refsection id="Contents_SUMMATION">
    <title>Contents</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="SUMMATION">SUMMATION Matrix Summation</link>
        </para>
      </listitem>
      <listitem>
        <itemizedlist>
          <listitem>
            <para>
              <xref linkend="Palette_SUMMATION">Palette</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Description_SUMMATION">Description</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Dialogbox_SUMMATION">Dialog box</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Defaultproperties_SUMMATION">Default properties</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Interfacingfunction_SUMMATION">Interfacing function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Computationalfunction_SUMMATION">Computational function</xref>
            </para>
          </listitem>
          <listitem>
            <para>
              <xref linkend="Authors_SUMMATION">Authors</xref>
            </para>
          </listitem>
        </itemizedlist>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Palette_SUMMATION">
    <title>Palette</title>
    <itemizedlist>
      <listitem>
        <para>
          <link linkend="Linear_pal">Linear - Linear palette</link>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Description_SUMMATION">
    <title>Description</title>
    <para>
The Sum block performs addition or subtraction on its inputs. This block can add or subtract scalar, vector, or matrix inputs. It can also collapse the elements of a single input vector.  
</para>
    <para>
The number of inputs is given by the second parameter. This parameter can be a vector of +1 and -1 or it can be a positive value. In the first case the size of the vector indicates the number of inputs and the signs indicates whether it is a summation or a subtraction. In the second case, the block is a summation block and the value indicates the number of inputs.  
</para>
    <para>
On overflow, the result can take different forms: 
</para>
    <para>
1- A normal non saturated result. 
</para>
    <para>
2- A saturated result. 
</para>
    <para>
3- An error message warning the user about the overflow. 
</para>
    <para>
The user can select one of these three forms by setting the "DO ON OVERFLOW" field to 0,1 or 2.  
</para>
    <para>
</para>
  </refsection>
  <refsection id="Dialogbox_SUMMATION">
    <title>Dialog box</title>
    <para>
      <inlinemediaobject>
        <imageobject>
          <imagedata fileref="../../../images/SUMMATION_gui.gif" align="center" valign="middle"/>
        </imageobject>
      </inlinemediaobject>
    </para>
    <para>

</para>
    <itemizedlist>
      <listitem>
        <para>
          <emphasis role="bold">Datatype (1=real double 2=complex 3=int32 ...)</emphasis>
        </para>
        <para> It indicates the type of the input/output data. It support all datatype, number must be between 1 and 8.</para>
        <para> Properties : Type 'vec' of size 1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Number of inputs or sign vector (of +1, -1)</emphasis>
        </para>
        <para> It indicates the number of inputs and the operation see the description for more detail.</para>
        <para> Properties : Type 'vec' of size -1. </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">Do on Overflow(0=Nothing 1=Saturate 2=Error)</emphasis>
        </para>
        <para> When this parameter is set to zero the result is similar to a normal summation of two integer matrix. When it is set to 1, on overflow the block saturate the result. When it is set to 2, on overflow an error message box appears. If the Data type is double or complex this parameter is not used.</para>
        <para> Properties : Type 'vec' of size 1.</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Defaultproperties_SUMMATION">
    <title>Default properties</title>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">always active:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">direct-feedthrough:</emphasis> yes</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">zero-crossing:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">mode:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular inputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 2 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">regular outputs:</emphasis>
        </para>
        <para>
          <emphasis role="bold">- port 1 : size [-1,-2] / type 1</emphasis>
        </para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation inputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">number/sizes of activation outputs:</emphasis> 0</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">continuous-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">object discrete-time state:</emphasis> no</para>
      </listitem>
      <listitem>
        <para>
          <emphasis role="bold">name of computational function:</emphasis>
          <emphasis role="italic">summation</emphasis>
        </para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Interfacingfunction_SUMMATION">
    <title>Interfacing function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/macros/Linear/SUMMATION.sci</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Computationalfunction_SUMMATION">
    <title>Computational function</title>
    <itemizedlist>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_z.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i32n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i16n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i8n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui32n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui16n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui8n.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i32s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i16s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i8s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui32s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui16s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui8s.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i32e.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i16e.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_i8e.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui32e.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui16e.c</para>
      </listitem>
      <listitem>
        <para> SCI/modules/scicos_blocks/src/c/summation_ui8e.c</para>
      </listitem>
    </itemizedlist>
  </refsection>
  <refsection id="Authors_SUMMATION">
    <title>Authors</title>
    <para>
      <emphasis role="bold"/>
    </para>
    <itemizedlist>
      <listitem>
        <para><emphasis role="bold">Fady NASSIF</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Alan Layec</emphasis> INRIA</para>
      </listitem>
      <listitem>
        <para><emphasis role="bold">Ramine Nikoukhah</emphasis> INRIA</para>
      </listitem>
    </itemizedlist>
  </refsection>
</refentry>
