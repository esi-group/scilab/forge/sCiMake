# Localization of the module scicos
# Please see in SCI/tools/localization for localization management
# Copyright (C) 2007/2008 - INRIA / Scilab
# This file is distributed under the same license as the Scilab package.
# Sylvestre Ledru <sylvestre.ledru@inria.fr>, 2007/2008.
#
msgid ""
msgstr ""
"Project-Id-Version: Scilab\n"
"Report-Msgid-Bugs-To: <localization@scilab.org>\n"
"POT-Creation-Date: 2008-09-22 14:07+0100\n"
"Last-Translator: Sylvestre Ledru <sylvestre.ledru@inria.fr>\n"
"Language-Team: Scilab Localization <localization@scilab.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=ISO-8859-1\n"
"Content-Transfer-Encoding: 8bit\n"
"Revision-Date: 2009-12-09 16:34+0100\n"

# File: macros/scicos_scicos/genmoc.sci, line: 79
#, c-format
msgid " %s.mo cannot be compiled\n"
msgstr ""
#
# File: macros/scicos_scicos/genmoc.sci, line: 55
#, c-format
msgid " %s.mo compilation forced\n"
msgstr ""
#
# File: macros/scicos_scicos/genmoc.sci, line: 76
#, c-format
msgid " %s.mo must be recompiled\n"
msgstr ""
#
# File: macros/scicos_scicos/MODCOM.sci, line: 83
#, c-format
msgid "%s: An error occurred during the compilation of the Modelica block."
msgstr ""
#
# File: macros/scicos_scicos/genmoc.sci, line: 45
#, c-format
msgid "%s: Cannot find any MO files in the following location: %s.\n"
msgstr ""
#
# File: macros/scicos_auto/lincos.sci, line: 126
#, c-format
msgid "%s: Diagram does not compile in pass %d.\n"
msgstr ""
#
# File: macros/scicos_auto/lincos.sci, line: 136
#, c-format
msgid ""
"%s: Diagram does not compile in pass %d.\n"
"'),"
msgstr ""
#
# File: macros/scicos_scicos/build_block.sci, line: 92
#, c-format
msgid "%s: Error : the following command line failed to execute: %s.\n"
msgstr ""
#
# File: macros/scicos_scicos/build_block.sci, line: 31
#, c-format
msgid "%s: Error: A scifunc block has not been defined."
msgstr ""
#
# File: macros/scicos_scicos/genmoc.sci, line: 32
# File: macros/scicos_scicos/build_block.sci, line: 83
# File: macros/scicos_scicos/MODCOM.sci, line: 77
# File: macros/scicos_scicos/compile_modelica.sci, line: 28
#, c-format
msgid "%s: Error: Modelica compiler (MODELICAC) is unavailable."
msgstr ""
#
# File: macros/scicos_auto/lincos.sci, line: 115
#, c-format
msgid "%s: Input ports are not numbered properly.\n"
msgstr ""
#
# File: macros/scicos_auto/lincos.sci, line: 120
#, c-format
msgid "%s: Output ports are not numbered properly.\n"
msgstr ""
#
# File: macros/scicos_auto/scicos_simulate.sci, line: 181
#, c-format
msgid "%s: Wrong number of input arguments. Must be between %d and %d.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 29
#, c-format
msgid "%s: Wrong number of input arguments: %d expected.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 32
#, c-format
msgid "%s: Wrong number of output arguments: %d expected.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 46
#, c-format
msgid "%s: Wrong size for input argument #%d: Single integer expected.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 43
#, c-format
msgid "%s: Wrong size for input argument #%d: Single string expected.\n"
msgstr ""
#
# File: macros/scicos_auto/lincos.sci, line: 180
#, c-format
msgid "%s: Wrong size for input arguments #%d and #%d.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 39
#, c-format
msgid "%s: Wrong type for input argument #%d: Integer expected.\n"
msgstr ""
#
# File: macros/scicos_scicos/xgetcolor.sci, line: 36
#, c-format
msgid "%s: Wrong type for input argument #%d: String expected.\n"
msgstr ""
#
# File: macros/scicos_scicos/do_load_as_palette.sci, line: 54
# File: macros/scicos_scicos/menu_stuff.sci, line: 28
# File: macros/scicos_scicos/do_palettes.sci, line: 92
msgid "&?"
msgstr ""
#
# File: macros/scicos_scicos/do_load_as_palette.sci, line: 52
# File: macros/scicos_scicos/menu_stuff.sci, line: 26
# File: macros/scicos_scicos/do_palettes.sci, line: 90
msgid "&Edit"
msgstr ""
#
# File: macros/scicos_scicos/menu_stuff.sci, line: 25
msgid "&File"
msgstr ""
#
# File: macros/scicos_scicos/do_load_as_palette.sci, line: 53
# File: macros/scicos_scicos/menu_stuff.sci, line: 27
# File: macros/scicos_scicos/do_palettes.sci, line: 91
msgid "&Tools"
msgstr ""
#
# File: macros/scicos_auto/scicos.sci, line: 41
msgid "''scicos'' is no more available, please use ''xcos'' instead.\n"
msgstr ""
#
# File: macros/scicos_scicos/buildnewblock.sci, line: 68
msgid "A compatible C compiler required."
msgstr ""
#
# File: macros/scicos_scicos/do_icon_edit.sci, line: 34
msgid "Only one block can be selected in current window for this operation."
msgstr ""
#
# File: macros/scicos_scicos/genmoc.sci, line: 95
#, c-format
msgid "Problem while building library %s\n"
msgstr ""
#
# File: macros/scicos_scicos/buildnewblock.sci, line: 68
msgid "Sorry compiling problem"
msgstr ""
#
# File: macros/scicos_scicos/do_icon_edit.sci, line: 67
msgid "The current icon depends on block parameter\n"
msgstr ""
