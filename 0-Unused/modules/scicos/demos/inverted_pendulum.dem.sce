//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

exec ("SCI/modules/xcos/demos/setw.sci");

// Open the cosf file with xcos
xcos("SCI/modules/scicos/demos/Inverted_pendulum.cosf");

