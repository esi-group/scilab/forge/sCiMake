//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

thispath = get_absolute_file_path("scilab_block.dem.sce");
// scicos_demostration(thispath+"/Scilab_Block.cosf");

//Open the cosf file with xcos
xcos(thispath+"/Scilab_Block.cosf");

clear thispath;
