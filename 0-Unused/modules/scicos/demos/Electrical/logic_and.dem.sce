//
// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008 - INRIA
//
// This file is distributed under the same license as the Scilab package.
//

thispath = get_absolute_file_path("logic_and.dem.sce");
xcos(thispath+"/AND_Gate.cosf");
clear thispath;
